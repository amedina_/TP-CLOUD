# TP 5 & 6 d'Architectures Distribuées

Vous aurez à rendre ce projet en binôme pour le **14 Mai 2020**. 
Tout retard sera pénalisé suivant une règle de calcul exponentielle (1 point, 3 points, 6 points, etc.).

LLe projet est à rendre par **email** sous la forme d'une adresse vers un **projet GITLAB privé** accessible depuis le web. 
Vous ajouterez vos encadrants (les comptes **ccaron** et **asaval** en tant que développeurs) pour qu'ils puissent avoir accès à votre projet en vous assurant que celui-ci contienne:
1. Les **noms et prénoms** de votre binôme dans un fichier **AUTEURS** à la racine
2. L'**adresse** publique de votre service déployé en PaaS
3. Le **code** de votre service RESTFUL
4. Une documentation de votre service dans un fichier **README** à la racine
5. Un **rapport** sur les problèmes rencontrés et les solutions que vous y avez apportés et l'intégration du système de persistence

Vous pouvez également télécharger une version [PDF de l'énoncé du TP](TP-CLOUD.pdf).
